<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>天气预报</title>
</head>
<style>
    *{padding:0;margin:0;}
    li{list-style: none;margin:2px 0;}
    table{
        border:solid 1px;
    }
    table td{
        padding:12px;
        border:solid 1px;
    }
    #mian{
        margin:100px 150px;
    }
    .img{text-align: center;}
    h1{
        padding:2px;margin:10px;
    }
    h2{width: 80px;padding-bottom:4px;}
    form {
        padding: 5px;
    }
    form span{
        padding:0 5px;
    }
    #city{padding:2px 5px;margin: 2px 10px 0 2px;background: #ddd; border:1px #999; width: 120px;}
    #todayInfo{margin:20px;}
    #btn{text-decoration: none;padding:2px;font: 12px;}
    #btn:hover{background: #ddd;color:#999;border-radius: 4px; padding: 2px 4px;}
</style>
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="./ajax.js"></script>
<body>
    <div id='mian'>
        <div>
            <h1>看天出行，快乐每一天</h1>
            <form  method='post'>
                <span>请输入你要查询的城市:</span>
                <input id='city' name='city' value='长沙' type="text" />
                <a id='btn' href="javascript:void(0);">查询</a>
            </form>
        </div>

        <!-- 当前城市天气情况： -->
        <div id='todayInfo'>
        </div>

        <div id='weekInfo'>
        </div>
    </div>
</body>
</html>